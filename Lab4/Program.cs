﻿//
//  Program.cs
//
//  Author:
//       Andrew Puhovsky <borlcand@gmail.com>
//
//  Copyright (c) 2016 BorlCand Limited
//
using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using Lab4.Classes;
using Lab4.Enums;

namespace Lab4
{
    class Program
    {
        static void Main(string[] args)
		{

			// PREPARING 1-2
			StudentCollection col1 = new StudentCollection ();
			StudentCollection col2 = new StudentCollection ();

			col1.Name = "Collection 1";
			col2.Name = "Collection 2";

			Journal j1 = new Journal ();
			Journal j2 = new Journal ();

			col1.StudentCountChanged += j1.StudentListChangedHandler;
			col1.StudentReferenceChanged += j1.StudentListChangedHandler;
			col1.StudentReferenceChanged += j2.StudentListChangedHandler;

			col2.StudentReferenceChanged += j2.StudentListChangedHandler;

			// 3

			col1.AddDefaults ();
			col2.AddDefaults ();

			col1 [1] = col2 [0];
			col2 [0] = col1 [2];

			col1.Remove (2);
			col2.Remove (2);

			// 4
			Console.WriteLine ("\nJournal 1\n");
			Console.WriteLine (j1.ToString());

			Console.WriteLine ("\nJournal 2\n");
			Console.WriteLine (j2.ToString());

			//end
            Console.ReadKey();
        }

    }
}
