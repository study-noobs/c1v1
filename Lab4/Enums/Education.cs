//
//  Education.cs
//
//  Author:
//       Andrew Puhovsky <borlcand@gmail.com>
//
//  Copyright (c) 2016 BorlCand Limited
//
using System;
using System.Collections.Generic;
using System.Text;

namespace Lab4.Enums
{
    public enum Education
    {
        //None,
        Specialist,
        Bachelor,
        SecondEducation
    }
}
