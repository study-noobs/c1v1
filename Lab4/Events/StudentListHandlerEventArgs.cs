﻿//
//  StudentListHandlerEventArgs.cs
//
//  Author:
//       Andrew Puhovsky <borlcand@gmail.com>
//
//  Copyright (c) 2016 BorlCand Limited
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab4.Classes;

namespace Lab4.Events
{

    public class StudentListHandlerEventArgs : EventArgs
    {
        public string Name { get; set; }
        public string ChangeType { get; set; }
        public Student ChangedStudent { get; set; }

        public StudentListHandlerEventArgs(string name, string changeType, Student student)
        {
            this.Name = name;
            this.ChangeType = changeType;
            this.ChangedStudent = student;
        }

        public override string ToString()
        {
            return String.Format("{0} changed. Change type is {1}. Changed object: {2}", this.Name, this.ChangeType, this.ChangedStudent.ToString());
        }
    }
}
