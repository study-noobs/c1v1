﻿//
//  JournalEntry.cs
//
//  Author:
//       Andrew Puhovsky <borlcand@gmail.com>
//
//  Copyright (c) 2016 BorlCand Limited
//
using System;

namespace Lab4.Classes
{
	public class JournalEntry
	{
		public string Name { get; set; }
		public string ChangeType { get; set; }
		public string StudentInfo { get; set; }

		public JournalEntry(string name, string changeType, string student)
		{
			this.Name = name;
			this.ChangeType = changeType;
			this.StudentInfo = student;
		}

		public override string ToString()
		{
			return String.Format("{0} changed. Change type is {1}. Changed object: {2}", this.Name, this.ChangeType, this.StudentInfo);
		}
	}
}

