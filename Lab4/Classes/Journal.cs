﻿//
//  Journal.cs
//
//  Author:
//       Andrew Puhovsky <borlcand@gmail.com>
//
//  Copyright (c) 2016 BorlCand Limited
//
using System;
using System.Collections.Generic;
using Lab4.Events;

namespace Lab4.Classes
{
	public class Journal
	{
		private List<JournalEntry> _journal = new List<JournalEntry>();

		public void StudentListChangedHandler(object source, StudentListHandlerEventArgs args)
		{
			this._journal.Add(new JournalEntry(args.Name, args.ChangeType, args.ChangedStudent.ToString()));
		}

		public override string ToString ()
		{
			return String.Join ("\r\n", Util.JournalListToStringArray(_journal));
		}
	}
}

