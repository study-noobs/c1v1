﻿//
//  TestCollections.cs
//
//  Author:
//       Andrew Puhovsky <borlcand@gmail.com>
//
//  Copyright (c) 2016 BorlCand Limited
//
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Lab4.Enums;

namespace Lab4.Classes
{
	[Serializable]
	public class TestCollections
	{
		private List<Person> _personList = new List<Person> ();
		private List<string> _stringList = new List<string> ();
		private Dictionary<Person, Student> _personStudentDictionary = new Dictionary<Person, Student> ();
		private Dictionary<string, Student> _stringStudentDictionary = new Dictionary<string, Student> ();

		public static TestCollections GetInitializedInstance (int count)
		{
			TestCollections instance = new TestCollections (count);

			for (int i = 0; i < count; i++) {
				instance._personList.Add(new Person ("Krot" + i, "Ivanov", new DateTime(1990,01,01)));
				instance._stringList.Add("text" + i);

				var prs = new Person ("Krot" + i, "Ivanov", new DateTime(1990,01,01));

				instance._personStudentDictionary[prs] = new Student (prs, Education.Bachelor, 59999);
				instance._stringStudentDictionary["text" + i] = new Student (new Person ("Krot" + i, "Ivanov", new DateTime(1990,01,01)), Education.Bachelor, 59999);
			}

			return instance;
		}

		public TestCollections (int count)
		{
			_personList = new List<Person> (count);
			_stringList = new List<string> (count);
			_personStudentDictionary = new Dictionary<Person, Student> (count);
			_stringStudentDictionary = new Dictionary<string, Student> (count);
		}

		public void DebugSearchTime (Person s_el1, string s_el2, Person s_key1, string s_key2, Student s_val1, Student s_val2)
		{
			Stopwatch sw;
			Console.WriteLine ();

			sw = Stopwatch.StartNew();
			_personList.Find( prs => prs.Equals(s_el1));
			sw.Stop();
			Console.WriteLine("Search time for Person element: " + sw.Elapsed);


			sw = Stopwatch.StartNew();
			_stringList.Find ( str => str.Equals(s_el2));
			sw.Stop();
			Console.WriteLine("Search time for string element: " + sw.Elapsed);


			sw = Stopwatch.StartNew();
			_personStudentDictionary.ContainsKey (s_key1);
			sw.Stop();
			Console.WriteLine("Search time for Person key: " + sw.Elapsed);

			sw = Stopwatch.StartNew();
			_personStudentDictionary.ContainsValue (s_val1);
			sw.Stop();
			Console.WriteLine("Search time for Student_In_Person value: " + sw.Elapsed);

			sw = Stopwatch.StartNew();
			_stringStudentDictionary.ContainsKey (s_key2);
			sw.Stop();
			Console.WriteLine("Search time for string key: " + sw.Elapsed);

			sw = Stopwatch.StartNew();
			_stringStudentDictionary.ContainsValue (s_val2);
			sw.Stop();
			Console.WriteLine("Search time for Student_In_string value: " + sw.Elapsed);
		}
	}
}

