﻿//
//  Util.cs
//
//  Author:
//       Andrew Puhovsky <borlcand@gmail.com>
//
//  Copyright (c) 2016 BorlCand Limited
//
using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;

namespace Lab4.Classes
{
    class Util
	{
		public static string[] ExamListToStringArray(List<Exam> list)
		{
			List<string> strings = new List<string>();

			foreach (Exam item in list)
			{
				strings.Add(item.ToShortString());
			}

			return strings.ToArray();
		}

		public static string[] TestListToStringArray(List<Test> list)
		{
			List<string> strings = new List<string>();

			foreach (Test item in list)
			{
				strings.Add(item.ToShortString());
			}

			return strings.ToArray();
		}

		public static string[] JournalListToStringArray(List<JournalEntry> list)
		{
			List<string> strings = new List<string>();

			foreach (JournalEntry item in list)
			{
				strings.Add(item.ToString());
			}

			return strings.ToArray();
		}
    }
}
