﻿//
//  IDateAndCopy.cs
//
//  Author:
//       Andrew Puhovsky <borlcand@gmail.com>
//
//  Copyright (c) 2016 BorlCand Limited
//
using System;

namespace Lab3.Interfaces
{
	public interface IDateAndCopy
	{
		object DeepCopy ();

		DateTime Date { get; set; }
	}

}

