﻿//
//  Student.cs
//
//  Author:
//       Andrew Puhovsky <borlcand@gmail.com>
//
//  Copyright (c) 2016 BorlCand Limited
//
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Lab3.Enums;
using Lab3.Interfaces;

namespace Lab3.Classes
{
	[Serializable]
	public class Student : Person, IDateAndCopy
    {
        private Education _education;
		private int _group;
		private List<Exam> _exams = new List<Exam>();
		private List<Test> _tests = new List<Test>();

		public Student(Person person, Education education, int group) : base(person.FirstName, person.LastName, person.Birthday)
        {
            this._education = education;
            this._group = group;
        }

		public Student() : base()
        {
            this._education = Education.Bachelor;
            this._group = 500;
        }

        public Person Person
        {
			get { return new Person(this._firstName, this._lastName, this._birthday); }
			set { 
				this._firstName = value.FirstName; 
				this._lastName = value.LastName; 
				this._birthday = value.Birthday; 
			}
        }

        public Education Education
        {
            get { return _education; }
            set { _education = value; }
        }

        public int Group
        {
            get { return _group; }
            set {
				if (value <= 100 || value > 599) {
					throw new ArgumentOutOfRangeException (string.Format("Group is '{0}'", value), "The value must be greater than 100 and less or equal 599");
				}

				_group = value; 
			}
        }

		public Exam[] Exams
		{
			get { return _exams.ToArray(); }
			set { _exams = new List<Exam>(value); }

		}

		public Test[] Tests
		{
			get { return _tests.ToArray(); }
			set { _tests = new List<Test>(value); }
		}

        public double AverageRate
        {
            get
            {
                if (_exams.Count > 0)
                {
                    int sum = 0;
                    foreach (Exam item in _exams)
                    {
                        sum += item.Mark;
                    }
                    return (double)sum / _exams.Count;
                }
                else
                {
                    return (double)0;
                }
            }
        }

        public bool this[Education index]
        {
            get
            {
                return this.Education == index;
            }
		}

		public void AddExams(params Exam[] exams)
		{
			this._exams.AddRange(exams);
		}

		public void AddTests(params Test[] tests)
		{
			this._tests.AddRange(tests);
		}

        public override string ToString()
        {
			return string.Format("{0} {1}, {2}, {3}, {4}", base.ToString(), _education, _group, string.Join(", ", Util.ExamListToStringArray(_exams)), string.Join(", ", Util.TestListToStringArray(_tests)));
        }

        public override string ToShortString()
        {
			return string.Format("{0} {1} {2} {3:0.00}", base.ToShortString(), this._education, this._group, this.AverageRate);
        }

		public override object DeepCopy ()
		{
			using (var ms = new MemoryStream ()) {
				var formatter = new BinaryFormatter ();
				formatter.Serialize (ms, this);
				ms.Position = 0;

				return (Student)formatter.Deserialize (ms);
			}
		}

		public IEnumerable<object> IterateSubjects()
		{
			foreach (var item in _exams) {
				yield return item;
			}

			foreach (var item in _tests) {
				yield return item;
			}
		}

		public IEnumerable<Exam> IterateMarksHigherThan(int mark)
		{
			foreach (Exam item in _exams) {
				if (item.Mark > mark) {
					yield return item;
				}
			}

		}

    }
}
