//
//  Program.cs
//
//  Author:
//       Andrew Puhovsky <borlcand@gmail.com>
//
//  Copyright (c) 2016 BorlCand Limited
//
using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using Lab3.Classes;
using Lab3.Enums;

namespace Lab3
{
	class Program
	{
		static void Main (string[] args)
		{
			StudentCollection students = new StudentCollection ();


			Student stud1 = new Student (
				new Person ("Sergey", "Ivanov", new DateTime (1995, 9, 13)),
				Education.SecondEducation,
				581065
			);

			stud1.AddExams (
				new Exam ("Math", 9, new DateTime (2016, 5, 23)),
				new Exam ("Programming", 10, new DateTime (2016, 5, 25)),
				new Exam ("Chemistry", 8, new DateTime (2016, 5, 30)),
				new Exam ("English", 10, new DateTime (2016, 6, 3))
			);


			Student stud2 = new Student (
				new Person ("Ivan", "Sergeevich", new DateTime (1995, 9, 10)),
				Education.Specialist,
				581064
			);

			stud2.AddExams (
				new Exam ("Math", 8, new DateTime (2016, 5, 23)),
				new Exam ("Programming", 5, new DateTime (2016, 5, 25)),
				new Exam ("Chemistry", 10, new DateTime (2016, 5, 30)),
				new Exam ("English", 4, new DateTime (2016, 6, 3))
			);

			Student stud3 = new Student (
				new Person ("Ivan", "Petrov", new DateTime (1995, 10, 13)),
				Education.Specialist,
				581064
			);

			stud3.AddExams (
				new Exam ("Math", 10, new DateTime (2016, 5, 23)),
				new Exam ("Programming", 9, new DateTime (2016, 5, 25)),
				new Exam ("Chemistry", 10, new DateTime (2016, 5, 30)),
				new Exam ("English", 9, new DateTime (2016, 6, 3))
			);

			Student stud4 = new Student (
				new Person ("Ivan", "Lutik", new DateTime (1994, 10, 13)),
				Education.Bachelor,
				581064
			);

			stud4.AddExams (
				new Exam ("Math", 9, new DateTime (2016, 5, 23)),
				new Exam ("Programming", 9, new DateTime (2016, 5, 25)),
				new Exam ("Chemistry", 10, new DateTime (2016, 5, 30)),
				new Exam ("English", 10, new DateTime (2016, 6, 3))
			);

			students.AddStudents (stud1, stud2, stud3, stud4);

			Console.WriteLine ("Created students list\n");
			Console.WriteLine (students.ToShortString ());


			Console.WriteLine ("\nSort by Lastname\n");
			students.SortByLastName ();
			Console.WriteLine (students.ToShortString ());

			Console.WriteLine ("\nSort by Birthday\n");
			students.SortByBirthday ();
			Console.WriteLine (students.ToString ());

			Console.WriteLine ("\nSort by AverageRate\n");
			students.SortByAverageRate ();
			Console.WriteLine (students.ToShortString ());


			Console.WriteLine ("\nMax average mark: " + students.GetMaxAverageMark);


			Console.WriteLine ("\nFilter by Specialist education\n");
			IEnumerable<Student> specialists = students.GetSpecialists;

			foreach (Student item in specialists) {
				Console.WriteLine (item);
			}

			Console.WriteLine ("\nGroup by avg mark 9.5\n");
			List<Student> groupedByAvgMark = students.GetStudentsGroupedByAverageMark (9.5);

			foreach (Student item in groupedByAvgMark) {
				Console.WriteLine (item.ToShortString ());
			}


			//search time!

			TestCollections testFirst = TestCollections.GetInitializedInstance (10000);

			Console.WriteLine ("\nSearch first element ((WARNING!) Timings may be increased BY CLR JIT COMPILATION because of first run)");
			testFirst.DebugSearchTime (
				new Person ("Krot0", "Ivanov", new DateTime (1990, 01, 01)), 
				"text0", 
				new Person ("Krot0", "Ivanov", new DateTime (1990, 01, 01)), 
				"text0", 
				new Student (new Person ("Krot0", "Ivanov", new DateTime (1990, 01, 01)), Education.Bachelor, 59999), 
				new Student (new Person ("Krot0", "Ivanov", new DateTime (1990, 01, 01)), Education.Bachelor, 59999)
			);
			Console.WriteLine ("\nSearch first element ((WARNING!) Timings are normal. Cached JIT-compiled code)");
			testFirst.DebugSearchTime (
				new Person ("Krot0", "Ivanov", new DateTime (1990, 01, 01)), 
				"text0", 
				new Person ("Krot0", "Ivanov", new DateTime (1990, 01, 01)), 
				"text0", 
				new Student (new Person ("Krot0", "Ivanov", new DateTime (1990, 01, 01)), Education.Bachelor, 59999), 
				new Student (new Person ("Krot0", "Ivanov", new DateTime (1990, 01, 01)), Education.Bachelor, 59999)
			);



			TestCollections testCentral = TestCollections.GetInitializedInstance (10000);
			Console.WriteLine ("\nSearch central element ((WARNING!) Timings may be increased BY CLR JIT COMPILATION because of first run)\"");
			testCentral.DebugSearchTime (
				new Person ("Krot5", "Ivanov", new DateTime (1990, 01, 01)), 
				"text5", 
				new Person ("Krot5", "Ivanov", new DateTime (1990, 01, 01)), 
				"text5", 
				new Student (new Person ("Krot5", "Ivanov", new DateTime (1990, 01, 01)), Education.Bachelor, 59999), 
				new Student (new Person ("Krot5", "Ivanov", new DateTime (1990, 01, 01)), Education.Bachelor, 59999)
			);
			Console.WriteLine ("\nSearch central element ((WARNING!) Timings are normal. Cached JIT-compiled code)");
			testCentral.DebugSearchTime (
				new Person ("Krot5", "Ivanov", new DateTime (1990, 01, 01)), 
				"text5", 
				new Person ("Krot5", "Ivanov", new DateTime (1990, 01, 01)), 
				"text5", 
				new Student (new Person ("Krot5", "Ivanov", new DateTime (1990, 01, 01)), Education.Bachelor, 59999), 
				new Student (new Person ("Krot5", "Ivanov", new DateTime (1990, 01, 01)), Education.Bachelor, 59999)
			);


			TestCollections testLast = TestCollections.GetInitializedInstance (10000);
			Console.WriteLine ("\nSearch last element ((WARNING!) Timings may be increased BY CLR JIT COMPILATION because of first run)\"");
			testLast.DebugSearchTime (
				new Person ("Krot9999", "Ivanov", new DateTime (1990, 01, 01)), 
				"text9999", 
				new Person ("Krot9999", "Ivanov", new DateTime (1990, 01, 01)), 
				"text9999",
				new Student (new Person ("Krot9999", "Ivanov", new DateTime (1990, 01, 01)), Education.Bachelor, 59999), 
				new Student (new Person ("Krot9999", "Ivanov", new DateTime (1990, 01, 01)), Education.Bachelor, 59999)
			);
			Console.WriteLine ("\nSearch last element ((WARNING!) Timings are normal. Cached JIT-compiled code)");
			testLast.DebugSearchTime (
				new Person ("Krot9999", "Ivanov", new DateTime (1990, 01, 01)), 
				"text9999", 
				new Person ("Krot9999", "Ivanov", new DateTime (1990, 01, 01)), 
				"text9999",
				new Student (new Person ("Krot9999", "Ivanov", new DateTime (1990, 01, 01)), Education.Bachelor, 59999), 
				new Student (new Person ("Krot9999", "Ivanov", new DateTime (1990, 01, 01)), Education.Bachelor, 59999)
			);


			TestCollections testNonexist = TestCollections.GetInitializedInstance (10000);
			Console.WriteLine ("\nSearch non-exist element ((WARNING!) Timings may be increased BY CLR JIT COMPILATION because of first run)\"");
			testNonexist.DebugSearchTime (
				new Person ("Krot10000", "Ivanov", new DateTime (1990, 01, 01)), 
				"text10000", 
				new Person ("Krot10000", "Ivanov", new DateTime (1990, 01, 01)), 
				"text10000", 
				new Student (new Person ("Krot10000", "Ivanov", new DateTime (1990, 01, 01)), Education.Bachelor, 59999), 
				new Student (new Person ("Krot10000", "Ivanov", new DateTime (1990, 01, 01)), Education.Bachelor, 59999)
			);
			Console.WriteLine ("\nSearch non-exist element ((WARNING!) Timings are normal. Cached JIT-compiled code)");
			testNonexist.DebugSearchTime (
				new Person ("Krot10000", "Ivanov", new DateTime (1990, 01, 01)), 
				"text10000", 
				new Person ("Krot10000", "Ivanov", new DateTime (1990, 01, 01)), 
				"text10000", 
				new Student (new Person ("Krot10000", "Ivanov", new DateTime (1990, 01, 01)), Education.Bachelor, 59999), 
				new Student (new Person ("Krot10000", "Ivanov", new DateTime (1990, 01, 01)), Education.Bachelor, 59999)
			);

			//end
			Console.ReadKey ();
		}
	}
}
