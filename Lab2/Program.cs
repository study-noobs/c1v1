//
//  Program.cs
//
//  Author:
//       Andrew Puhovsky <borlcand@gmail.com>
//
//  Copyright (c) 2016 BorlCand Limited
//
using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace Lab2
{
    class Program
    {
        static void Main(string[] args)
        {
			// 1
			Console.WriteLine("---1---");
			Person prs1 = new Person("Sergey", "Ivanov", new DateTime(1995, 9, 13));
			Person prs2 = new Person("Sergey", "Ivanov", new DateTime(1995, 9, 13));

			Console.WriteLine(ReferenceEquals(prs1, prs2));
			Console.WriteLine(prs1 == prs2);

			Console.WriteLine(prs1.GetHashCode());
			Console.WriteLine(prs2.GetHashCode());

			// 2
			Console.WriteLine("---2---");
            Student stud = new Student(
                new Person("Sergey", "Ivanov", new DateTime(1995, 9, 13)),
                Education.SecondEducation,
                581065
                );

			stud.AddExams(
				new Exam("Math", 9, new DateTime(2016, 5, 23)),
				new Exam("Programming", 10, new DateTime(2016, 5, 25)),
				new Exam("Chemistry", 8, new DateTime(2016, 5, 30)),
				new Exam("English", 10, new DateTime(2016, 6, 3))
			);

			stud.AddTests(
				new Test("Math", true),
				new Test("OOP", true),
				new Test("Russian", false)
			);

            Console.WriteLine(stud.ToString());
            Console.WriteLine(stud.ToShortString());


			// 3
			Console.WriteLine("---3---");
			Console.WriteLine(stud.Person.GetType());


			// 4
			Console.WriteLine("---4---");

			Student studCopy = (Student) stud.DeepCopy ();
			stud.Year = 2000;
			Console.WriteLine("ORIGINAL - " + stud.ToString());
			Console.WriteLine("COPY - " + studCopy.ToString());


			// 5
			Console.WriteLine("---5---");
			try {
				stud.Group = 600;
			} catch (Exception ex) {
				Console.WriteLine (ex.Message);
			}


			// 6
			Console.WriteLine("---6---");

			foreach (var item in stud.IterateSubjects()) {
				Console.WriteLine (item);
			}

			foreach (var item in stud.IterateMarksHigherThan(3)) {
				Console.WriteLine (item);
			}

		
            //end
            Console.ReadKey();
        }
    }
}
