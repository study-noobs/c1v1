//
//  Exam.cs
//
//  Author:
//       Andrew Puhovsky <borlcand@gmail.com>
//
//  Copyright (c) 2016 BorlCand Limited
//
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Lab2
{	
	[Serializable]
	public class Exam : IDateAndCopy
    {
        public string Title { get; private set; }
        public int Mark { get; private set; }
        public DateTime ExamDate { get; private set; }
		public DateTime Date { get; set; }

        public Exam(string title, int mark, DateTime date)
        {
            Title = title;
            Mark = mark;
            ExamDate = date;
        }

        public Exam() : this("Default", 5, new DateTime(2016, 5, 12)) { }

        public override string ToString()
        {
            return string.Format("Student passed an exam {0} on {1} for a grade {2}", Title, ExamDate.ToShortDateString(), Mark);
        }

		public virtual string ToShortString()
        {
            return string.Format("({1}) {0} - mark {2}", Title, ExamDate.ToShortDateString(), Mark);
        }



		public virtual object DeepCopy ()
		{
			using (var ms = new MemoryStream ()) {
				var formatter = new BinaryFormatter ();
				formatter.Serialize (ms, this);
				ms.Position = 0;

				return (Exam)formatter.Deserialize (ms);
			}
		}
    }
}
