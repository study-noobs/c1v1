﻿//
//  StudentComparer.cs
//
//  Author:
//       Andrew Puhovsky <borlcand@gmail.com>
//
//  Copyright (c) 2016 BorlCand Limited
//
using System;
using System.Collections.Generic;
using System.Text;

namespace Lab5.Classes
{
	//Рекомендуется создавать производные от класса Comparer<T>, а не реализовывать интерфейс IComparer<T>, 
	//потому что класс Comparer<T> предоставляет явно реализующий интерфейс метод IComparer.Compare и 
	//свойство Default, которое получает компаратор по умолчанию для объекта.
	//https://msdn.microsoft.com/ru-ru/library/8ehhxeaf(v=vs.110).aspx
	class StudentComparer : Comparer<Student>
	{
		public override int Compare(Student x, Student y)
		{
			if (x.AverageRate < y.AverageRate)
			{
				return -1;
			}

			if (x.AverageRate > y.AverageRate)
			{
				return 1;
			}


			return 0;
		}
	}
}
