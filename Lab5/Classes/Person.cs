//
//  Person.cs
//
//  Author:
//       Andrew Puhovsky <borlcand@gmail.com>
//
//  Copyright (c) 2016 BorlCand Limited
//
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Lab5.Interfaces;

namespace Lab5.Classes
{
	[Serializable]
	public class Person : IDateAndCopy, IComparable, IComparer<Person>
	{
		protected string _firstName;
		protected string _lastName;
		protected DateTime _birthday;

		public Person (string fname, string lname, DateTime bday)
		{
			this._firstName = fname;
			this._lastName = lname;
			this._birthday = bday;
		}

		public Person () : this ("John", "Doe", new DateTime (1990, 10, 5)) {}


		#region Properties
		public string FirstName {
			get { return this._firstName; }
		}

		public string LastName {
			get { return this._lastName; }
		}

		public DateTime Birthday {
			get { return this._birthday; }
		}

		public int Year {
			get { return this._birthday.Year; }
			set { this._birthday = new DateTime (value, this._birthday.Month, this._birthday.Day); }
		}

		public DateTime Date { get; set; }
		#endregion


		#region Methods

		/// <summary>
		/// Determines whether the specified <see cref="System.Object"/> is equal to the current <see cref="Person"/>.
		/// Why I've done 2 methods? See https://msdn.microsoft.com/en-us/library/ms182358.aspx
		/// </summary>
		/// <param name="obj">The <see cref="System.Object"/> to compare with the current <see cref="Person"/>.</param>
		/// <returns><c>true</c> if the specified <see cref="System.Object"/> is equal to the current <see cref="Person"/>;
		/// otherwise, <c>false</c>.</returns>
		public override bool Equals (object obj)
		{
			if (!(obj is Person) || obj == null) {
				return false;
			}

			return Equals ((Person)obj);
		}

		public bool Equals (Person person)
		{
			if (ReferenceEquals(person, null)) {
				return false;
			}
			if (!this.FirstName.Equals (person.FirstName)) {
				return false;
			}
			if (!this.LastName.Equals (person.LastName)) {
				return false;
			}
			if (!this.Birthday.ToShortDateString ().Equals (person.Birthday.ToShortDateString ())) {
				return false;
			}

			return true;
		}

		public static bool operator == (Person obj1, Person obj2)
		{
			return obj1.Equals (obj2);
		}

		public static bool operator != (Person obj1, Person obj2)
		{
			return !obj1.Equals (obj2);
		}

		public override int GetHashCode ()
		{
			return FirstName.GetHashCode () ^ LastName.GetHashCode () ^ Birthday.ToShortDateString ().GetHashCode ();
		}

	    public virtual object DeepCopy ()
		{
			using (var ms = new MemoryStream ()) {
				var formatter = new BinaryFormatter ();
				formatter.Serialize (ms, this);
				ms.Position = 0;

				return (Person)formatter.Deserialize (ms);
			}
		}

		public int Compare(Person x, Person y)
		{
			return DateTime.Compare(x.Birthday, y.Birthday);
		}

		public int CompareTo(object obj)
		{
			if (!(obj is Person) || obj == null)
			{
				return 1;
			}

			return String.Compare(this.LastName, ((Person)obj).LastName);
		}

	    public override string ToString ()
		{
			return string.Format ("{0} {1}; Birthday - {2}", this._firstName, this._lastName, this._birthday.ToShortDateString ()); 
		}
			
		public virtual string ToShortString ()
		{
			return string.Format ("{0} {1}", this._firstName, this._lastName);
		}

		#endregion
	}
}
