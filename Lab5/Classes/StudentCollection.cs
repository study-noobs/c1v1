﻿//
//  StudentCollection.cs
//
//  Author:
//       Andrew Puhovsky <borlcand@gmail.com>
//
//  Copyright (c) 2016 BorlCand Limited
//
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab5.Enums;
using Lab5.Events;

namespace Lab5.Classes
{
    
	public class StudentCollection
	{
        public delegate void StudentListHandler(object source, StudentListHandlerEventArgs args);

        public event StudentListHandler StudentCountChanged;
        public event StudentListHandler StudentReferenceChanged;
    
		private readonly List<Student> _collection = new List<Student> ();

        public string Name { get; set; }

		public void AddDefaults ()
		{
			this.AddStudents (new Student (), new Student (new Person ("FTest1", "LTest1", new DateTime (2000, 12, 09)), Education.Specialist, 400), new Student (new Person ("FTest2", "LTest2", new DateTime (1999, 11, 03)), Education.SecondEducation, 343));
		}

		public void AddStudents (params Student[] students)
		{
			this._collection.AddRange (students);

		    foreach (Student student in students)
		    {
				if (StudentCountChanged != null) {
					StudentCountChanged (this, new StudentListHandlerEventArgs (Name, "StudentAdd", student));
				}
		    }
		}

		public override string ToString ()
		{
			List<string> strings = new List<string> ();

			foreach (Student student in _collection) {
				strings.Add (student.ToString ());
			}

			return String.Join ("\r\n", strings.ToArray ());
		}

		public string ToShortString ()
		{
			List<string> strings = new List<string> ();

			foreach (Student student in _collection) {
				strings.Add (student.ToShortString ());
			}

			return String.Join ("\r\n", strings.ToArray ());
		}

		public void SortByLastName ()
		{
			this._collection.Sort ();
		}

		public void SortByBirthday ()
		{
			this._collection.Sort (comparer: (IComparer<Student>) new Person ());
		}

		public void SortByAverageRate ()
		{
			this._collection.Sort (new StudentComparer ());
		}

		public double GetMaxAverageMark {
			get { return (double)this._collection.Max (student => student.AverageRate); }
		}

		public IEnumerable<Student> GetSpecialists {
			get { return this._collection.Where (student => student.Education == Education.Specialist); }
		}

		public List<Student> GetStudentsGroupedByAverageMark (double value)
		{
			return this._collection.GroupBy(student => student.AverageRate).Where(student => student.Key == value).SelectMany(v => v).ToList();
		}

	    public bool Remove(int index)
	    {
			if (this._collection[index] == null) return false;

			if (StudentCountChanged != null) {
				StudentCountChanged (this, new StudentListHandlerEventArgs (Name, "StudentRemove", this._collection [index]));
			}

			this._collection.RemoveAt(index);
	     
            return true;
	    }

	    public Student this[int index]
	    {
            get { return this._collection[index]; }
	        set
	        {
	            if (!ReferenceEquals(this._collection[index], value))
	            {
					if (StudentReferenceChanged != null) {
						StudentReferenceChanged (this, new StudentListHandlerEventArgs (this.Name, "StudentReferenceChanged", value));
					}
	            }
	            this._collection[index] = value;
	        }
	    }
	}
}
