﻿//
//  Test.cs
//
//  Author:
//       Andrew Puhovsky <borlcand@gmail.com>
//
//  Copyright (c) 2016 BorlCand Limited
//
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Lab5.Classes
{
	[Serializable]
	public class Test
	{
		public string Title { get; set;}
		public bool Success { get; set;}

		public Test (string title, bool success){
			this.Title = title;
			this.Success = success;
		}

		public Test () : this("Overwhelming", true) {}

		public override string ToString ()
		{
			return string.Format ("[Test {0} - {1}]", Title, Success);
		}

		public virtual string ToShortString ()
		{
			return string.Format ("[{0}] {1}", Title, Success);
		}

	}
}

