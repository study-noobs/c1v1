﻿//
//  Program.cs
//
//  Author:
//       Andrew Puhovsky <borlcand@gmail.com>
//
//  Copyright (c) 2016 BorlCand Limited
//
using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using Lab5.Classes;
using Lab5.Enums;

namespace Lab5
{
    class Program
    {
        static void Main(string[] args)
		{
			//1
			Console.WriteLine("--1--");

			Student student = new Student (
				new Person ("Sergey", "Ivanov", new DateTime (1995, 9, 13)),
				Education.SecondEducation,
				581065
			);

			student.AddExams (
				new Exam ("Math", 9, new DateTime (2016, 5, 23)),
				new Exam ("Programming", 10, new DateTime (2016, 5, 25)),
				new Exam ("Chemistry", 8, new DateTime (2016, 5, 30)),
				new Exam ("English", 10, new DateTime (2016, 6, 3))
			);

			Student studentCopy = (Student) student.DeepCopy();


			Console.WriteLine (student.ToString ());
			Console.WriteLine (studentCopy.ToString ());

			//2
			Console.WriteLine("--2--");

			Console.Write ("Enter filename: ");

			string fname;
			while ((fname = Console.ReadLine ()).Length == 0) 
			{
				Console.Write ("Enter filename: ");
			}

			if (File.Exists (fname)) {
				student.Load (fname);
				Console.WriteLine ("Loaded file");
			} else {
				try {
					using (File.Create (fname)){}	
					Console.WriteLine("Created new file");
				} catch (Exception ex) {
					Console.WriteLine(String.Format("Exception occured while creating file! Ex: {0}", ex.Message));
				}
			}

			//3
			Console.WriteLine("--3--");

			Console.WriteLine (student.ToString ());

			//4
			Console.WriteLine("--4--");

			student.AddFromConsole();
			student.Save (fname);

			Console.WriteLine (student.ToString ());

			//5
			Console.WriteLine("--5--");

			Student.Load(fname, ref student);
			student.AddFromConsole ();
			Student.Save (fname, student);

			//6
			Console.WriteLine("--6--");

			Console.WriteLine (student.ToString ());

			//end
			Console.WriteLine("--end--");

            Console.ReadKey();
        }

    }
}
