﻿//
//  Student.cs
//
//  Author:
//       Andrew Puhovsky <borlcand@gmail.com>
//
//  Copyright (c) 2016 BorlCand Limited
//
using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1
{
    class Student
    {
        private Person _person;
        private Education _education;
        private int _group;
        private List<Exam> _exams = new List<Exam>();

        public Student(Person person, Education education, int group)
        {
            this._person = person;
            this._education = education;
            this._group = group;
        }

        public Student()
        {
            this._person = new Person();
            this._education = Education.Bachelor;
            this._group = 581065;
        }

        public Person Person
        {
            get { return _person; }
            set { _person = value; }
        }

        public Education Education
        {
            get { return _education; }
            set { _education = value; }
        }

        public int Group
        {
            get { return _group; }
            set { _group = value; }
        }

        public Exam[] Exams
        {
            get { return _exams.ToArray(); }

        }

        public double AverageRate
        {
            get
            {
                if (_exams.Count > 0)
                {
                    int sum = 0;
                    foreach (Exam item in _exams)
                    {
                        sum += item.Mark;
                    }
                    return (double)sum / _exams.Count;
                }
                else
                {
                    return (double)0;
                }
            }
        }

        public bool this[Education index]
        {
            get
            {
                return this.Education == index;
            }
        }

        public void AddExams(params Exam[] exams)
        {
            this._exams.AddRange(exams);
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}", _person, _education, _group, string.Join(", ", Util.ExamListToStringArray(_exams)));
        }

        public virtual string ToShortString()
        {
            return string.Format("{0} {1} {2} {3:0.00}", this._person, this._education, this._group, this.AverageRate);
        }
    }
}
