//
//  Exam.cs
//
//  Author:
//       Andrew Puhovsky <borlcand@gmail.com>
//
//  Copyright (c) 2016 BorlCand Limited
//
using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1
{
    public class Exam : Object
    {
        public string Title { get; private set; }
        public int Mark { get; private set; }
        public DateTime Date { get; private set; }


        public Exam(string title, int mark, DateTime date)
        {
            Title = title;
            Mark = mark;
            Date = date;
        }

        public Exam() : this("Default", 5, new DateTime(2016, 5, 12)) { }

        public override string ToString()
        {
            return string.Format("Student passed an exam {0} on {1} for a grade {2}", Title, Date.ToShortDateString(), Mark);
        }

        public virtual string ToShortString()
        {
            return string.Format("({1}) {0} - mark {2}", Title, Date.ToShortDateString(), Mark);
        }
    }
}
