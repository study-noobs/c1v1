//
//  Program.cs
//
//  Author:
//       Andrew Puhovsky <borlcand@gmail.com>
//
//  Copyright (c) 2016 BorlCand Limited
//
using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace Lab1
{
    class Program
    {
        static void Main(string[] args)
        {

            // 1
            Student stud = new Student(
                new Person("Sergey", "Ivanov", new DateTime(1995, 9, 13)),
                Education.SecondEducation,
                581065
                );
            Console.WriteLine(stud.ToShortString());

            // 2
            Console.WriteLine(stud[Education.Bachelor]);
            Console.WriteLine(stud[Education.Specialist]);
            Console.WriteLine(stud[Education.SecondEducation]);

            // 3
            stud.Person = new Person("Ivan", "Sergeev", new DateTime(1994, 11, 23));
            stud.Group = 581065;
            stud.Education = Education.Specialist;

            Console.WriteLine(stud.ToString());

            // 4
            stud.AddExams(
                new Exam("Math", 9, new DateTime(2016, 5, 23)),
                new Exam("Programming", 10, new DateTime(2016, 5, 25)),
                new Exam("Chemistry", 8, new DateTime(2016, 5, 30)),
                new Exam("English", 10, new DateTime(2016, 6, 3))
                );

            Console.WriteLine(stud.ToString());

            // 1
            Console.WriteLine(stud.ToShortString());

            // 5
            // Init new arrays
            Exam[] simpleArray = new Exam[1000000];
            Exam[,] doubleArray = new Exam[1000, 1000];
            Exam[][] dynamicDoubleArray = new Exam[1000][];

            for (int i = 0; i < dynamicDoubleArray.Length; i++)
                dynamicDoubleArray[i] = new Exam[1000];

            //test1 (linear)
            Stopwatch sw = Stopwatch.StartNew();

            for (int i = 0; i < 1000000; i++)
                simpleArray[i] = null;

            sw.Stop();
            Console.WriteLine(sw.Elapsed);

            //test2 (rectangle)
            sw = Stopwatch.StartNew();

            for (int i = 0; i < 1000; i++)
                for (int j = 0; j < 1000; j++)
                    doubleArray[i, j] = null;

            sw.Stop();
            Console.WriteLine(sw.Elapsed);

            //test3 (jagged)
            sw = Stopwatch.StartNew();

            for (int i = 0; i < 1000; i++)
                for (int j = 0; j < 1000; j++)
                    dynamicDoubleArray[i][j] = null;

            sw.Stop();
            Console.WriteLine(sw.Elapsed);

            //end
            Console.ReadKey();
        }
    }
}
