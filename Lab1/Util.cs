﻿//
//  Util.cs
//
//  Author:
//       Andrew Puhovsky <borlcand@gmail.com>
//
//  Copyright (c) 2016 BorlCand Limited
//
using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1
{
    class Util
    {
        public static string[] ExamListToStringArray(List<Exam> list)
        {
            List<string> strings = new List<string>();

            foreach (Exam item in list)
            {
                strings.Add(item.ToShortString());
            }

            return strings.ToArray();
        }
    }
}
