//
//  Person.cs
//
//  Author:
//       Andrew Puhovsky <borlcand@gmail.com>
//
//  Copyright (c) 2016 BorlCand Limited
//
using System;
using System.Collections.Generic;
using System.Text;

namespace Lab1
{
    class Person
    {
        private string _firstName;
        private string _lastName;
        private DateTime _birthday;

        public Person(string fname, string lname, DateTime bday)
        {
            this._firstName = fname;
            this._lastName = lname;
            this._birthday = bday;
        }

        public Person() : this("John", "Doe", new DateTime(1990, 10, 5)) { }

        public string FirstName {
            get { return this._firstName; }
        }

        public string LastName
        {
            get { return this._lastName; }
        }

        public DateTime Birthday
        {
            get { return this._birthday; }
        }

        public int Year
        {
            get { return this._birthday.Year; }
            set { this._birthday = new DateTime(value, this._birthday.Month, this._birthday.Day); }
        }


        public override string ToString() 
        {
            return string.Format("{0} {1}; Birthday - {2}", this._firstName, this._lastName, this._birthday.ToShortDateString()); 
        }

        public virtual string ToShortString()
        {
            return string.Format("{0} {1}", this._firstName, this._lastName);
        }
    }
}
